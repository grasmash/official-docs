[[guide_evaluator]]
=== Goals

This guide provides instructions for creating a temporary Drupal _demo_ application that can be used to evaluate Drupal on your local machine. The demo application is unsuitable for live websites and should be used for demonstration purposes only.

There are many alternative methods of creating a Drupal site. This guide aims to provide one way that works for most people.

[TIP]
Read the link:https://www.drupal.org/docs/user_guide/en/index.html[Drupal 8 User Guide] if you'd prefer to begin building a custom Drupal application that can be used for a live website.

[TIP]
If you cannot access the command line interface or are not comfortable using it to install software as described below, consider link:https://www.drupal.org/try-drupal[trying a free, hosted solution] for your evaluation.

=== Requirements

In order to install and run a Drupal demo application on your local machine, your system must include:

* An available command line interface
* https://www.drupal.org/docs/8/system-requirements/php-requirements[PHP 5.5.9+] (included with MacOS)

To verify that you have PHP installed and that it meets the minimum version requirement, you can run `php -v` in your command line interface.

=== Download Drupal

To download Drupal, open your command line interface.

*For Mac & Linux*, execute the following commands:

[source,bash]
----
mkdir drupal
cd drupal
curl -sSL https://www.drupal.org/download-latest/tar.gz | tar -xz --strip-components=1
----

These commands will create a new _drupal_ directory, change into it, and download and decompress a compressed copy of the link:/8/download[latest recommended version of Drupal] into that new directory.

*For Windows*

Manually download the link:https://www.drupal.org/download-latest/zip[zip file for the latest recommended version of Drupal] and decompress it. `cd` the into the resultant directory on the command line.

=== Install Drupal & Login

Next, execute the following command:

[source,bash]
----
php core/scripts/drupal quick-start demo_umami
----

This will start a link:http://php.net/manual/en/features.commandline.webserver.php[built-in PHP webserver], install Drupal's link:https://www.drupal.org/docs/8/umami-drupal-8-demonstration-installation-profile[Umami profile] to a link:http://php.net/manual/en/sqlite3.installation.php[SQLite database], and open your web browser to login to the new Drupal site.

If you'd like to learn more about Drupal's built-in `quick-start` command, execute:

[source,bash]
----
php core/scripts/drupal quick-start --help
----

=== What's Next

You're now logged in (as an administrator) to the demo application. You are free to experiment with Drupal and acquaint yourself with its many features.

When you're ready to get started with building your own Drupal application, visit the link:https://www.drupal.org/docs/user_guide/en/index.html[Drupal 8 User Guide].

You can destroy the demo application and its database by simply deleting the application directory.